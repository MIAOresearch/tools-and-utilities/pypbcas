from pbcas.ast import *
from pbcas.normalized import Variable as NormalizedVar
from copy import deepcopy

class InvalidNode(ValueError):
    pass

class CNFPrinter(FancyVisitor):
    def __init__(self):
        super().__init__()
        self.result = ""

    def in_add(self, value):
        self.result += " "

    def pre_variable(self, value):
        self.result += str(value.name)
        return False

    def pre_neg(self, value):
        self.result += "-"

    def pre_geq(self, value):
        pass

    def pre_add(self, value):
        pass

    def pre_mult(self, value):
        pass

    def pre_integer(self, value):
        if value.value != 1:
            raise InvalidNode("Can't put non 1 coefficients into cnf.")
        return False

    def pre_int(self, value):
        if value != 1:
            raise InvalidNode("Can't put non 1 coefficients into cnf.")
        return False

    def pre_default(self, value):
        print(value.__class__.__name__)
        raise InvalidNode()

@TimedFunction.time("asCNF")
def asCNF(ast, normalized):
    if hasattr(ast, "asCNF"):
        return ast.asCNF()

    if not normalized and not getattr(ast, "isNormalized", False):
        ast = normalize_by_reduce(ast)

    if not isinstance(ast.rhs, int):
        raise InvalidNode("Can't convert to CNF, right hand side is non integer.")

    if ast.rhs > 1:
        raise ValueError("Trying to add non clause to cnf.")
    elif ast.rhs == 1:
        visitor = CNFPrinter()
        travers(ast, visitor)

        return visitor.result + " 0"
    else:
        return None


class CNFFormula(NaryOp):
    def __init__(self, constraints = None, rename = lambda x: x):
        if constraints is None:
            constraints = []
        super().__init__(constraints)
        self.rename = rename

    @classmethod
    def read(cls, file):
        formula = cls()
        header = next(file)
        for line in file:
            if line[0] == "c":
                continue
            int_lits = list(map(int, line.split(" ")))
            assert(int_lits[-1] == 0)
            lits = [Variable(x) if x > 0 else ~Variable(abs(x)) for x in int_lits[:-1]]
            C = sum(lits) >= Integer(1)
            formula.add(C)
        return formula

    def add(self, constraint):

        if not getattr(constraint, "isNormalized", False):
            constraint = normalize_by_reduce(constraint)

        for a, x in constraint.lhs:
            if a > 1:
                print_asci(constraint)
                raise ValueError("Trying to add non 1 coefficient to cnf.")

        if constraint.rhs > 1:
            raise ValueError("Trying to add non clause to cnf.")
        elif constraint.rhs == 1:
            self.values.append(constraint)

    def write(self, file, startConstraintId = 1):
        numConstraints = len(self.values)
        numVars = len(collect_variables(self))

        print("p cnf %i %i"%(numVars, numConstraints), file=file)

        constraintId = startConstraintId
        for constraint in self.values:
            C = asCNF(self.rename(constraint), normalized = True)
            if C is not None:
                print(C, file = file)
            if constraintId is not None:
                constraint.constraintId = constraintId
                constraintId += 1

    def getConstraints(self):
        return self.values

    def num_constraints(self):
        return len(self.values)