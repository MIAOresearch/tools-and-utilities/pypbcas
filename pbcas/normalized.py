import pbcas.ast
from copy import copy


def Variable(name):
    return mkNormalizedVar(pbcas.ast.Variable(name))

def mkNormalizedVar(variable):
    res = NormalizedLinearSum()
    res.data[variable] = 1
    return res

def Integer(value):
    res = NormalizedLinearSum()
    res.data[None] = value
    return res

def geqCombine(lhs, rhs):
    result = copy(lhs)
    for key, val in rhs.items():
        result[key] = result.get(key, 0) + (-1 * val)

    return result

def normalize(constr):
    return constr

def negate(constr):
    # for a constraint (>=, <= or =) we get {None: -rhs, x_i:
    # a_i} meaning sum(a_i * x_i) >= rhs and want to produce
    # sum(-a_i * x_i) >= -rhs + 1, i.e., {None: rhs - 1, x_i:
    # -a_i}

    result = dict()
    for key, value in constr.items():
        result[key] = -1 * value

    result[None] = result.get(None, 0) - 1
    return result

def computeNormalizedRhs(constr):
    result = 0
    for key, value in constr.items():
        if key is None:
            result -= value
        else:
            if value < 0:
                result -= value
    return result


class NormalizedLinearSum:
    def __init__(self, data = None):
        if data is None:
            self.data = dict()
        else:
            self.data = data

        self.isConstraint = False
        self.op = None
        self.isNormalized = True

    def copyState(self, other):
        self.isConstraint = other.isConstraint
        self.op = other.op

    def __repr__(self):
        return  "NormalizedLinearSum(" + str(self.data) + ")"

    @property
    def isGeq(self):
        return self.op == ">="

    def __iter__(self):
        raise NotImplementedError()

    def __bool__(self):
        raise ValueError("Can not convert NormalizedLinearSum to bool.")

    def __str__(self):
        return str(self.data)

    def __add__(self, other):
        if not self.isConstraint:
            if isinstance(other, int):
                otherData = { None : other }
            elif isinstance(other, NormalizedLinearSum):
                otherData = other.data
            else:
                raise ValueError("Trying to add things I don't understand.")

            result = copy(self.data)
            for key, val in otherData.items():
                result[key] = val + self.data.get(key, 0)
            return NormalizedLinearSum(result)
        else:
            return pbcas.ast.Add([self, other])

    def __mul__(self, other):
        if not self.isConstraint:
            if isinstance(other, int):
                factor = other
            elif isinstance(other, NormalizedLinearSum):
                raise NotImplementedError()

            result = deepcopy(self.data)

            for key, val in result.items():
                result[key] = val * factor

            return NormalizedLinearSum(result)
        else:
            return pbcas.ast.Mult([self, other])

    def __rmul__(self, other):
        return self.__mul__(other)

    def __radd__(self, other):
        return self.__add__(other)

    def __truediv__(self, other):
        if self.isConstraint:
            return Div(self,other)
        else:
            raise ValueError("Can't divide linear sum.")

    def __ge__(self, other):
        if isinstance(other, int):
            otherData = {None: other}
        elif isinstance(other, NormalizedLinearSum):
            otherData = other.data
        else:
            raise ValueError("Trying to >= things I don't understand.")

        result = NormalizedLinearSum(geqCombine(self.data, otherData))
        result.isConstraint = True
        result.op = ">="
        return result

    def __le__(self, other):
        raise NotImplementedError()

    def __lt__(self, other):
        raise NotImplementedError()

    def __gt__(self, other):
        raise NotImplementedError()


    def __rshift__(self, other):
        if len(self.data) != 1:
            raise NotImplementedError()

        if not other.isConstraint:
            raise ValueError("Can't imply linear sum.")

        for x,i in self.data.items():
            if i != 1:
                raise NotImplementedError()
            var = x

        result = copy(other.data)
        degree = computeNormalizedRhs(result)
        result[var] = result.get(var, 0) - degree
        result[None] = result.get(None, 0) + degree

        result = NormalizedLinearSum(result)


        result.implyingLit = next(iter(self.data.keys()))
        result.copyState(other)
        assert(result.isConstraint)
        return result

    def __lshift__(self,other):
        if len(self.data) != 1:
            raise NotImplementedError()

        if not other.isConstraint:
            raise ValueError("Can't imply linear sum.")

        for x,i in self.data.items():
            if i != 1:
                raise NotImplementedError()
            var = x

        result = negate(other.data)
        degree = computeNormalizedRhs(result)
        result[var] = result.get(var, 0) + degree

        result = NormalizedLinearSum(result)
        result.implyingLit = ~next(iter(self.data.keys()))
        result.copyState(other)
        assert(result.isConstraint)
        return result

    def __invert__(self):
        if self.isConstraint:
            result = NormalizedLinearSum(negate(self.data))
            result.copyState(self)
            return result
        elif len(self.data) == 1 and None in self.data:
            if self.data[None] not in [0,1]:
                raise ValueError("Can only negate 0-1 integer.")
            return NormalizedLinearSum({None: 1 - self.data[None]})
        else:
            # Switch literals, i.e.,
            # x -> 1-x
            # 1-x -> x
            result = dict()
            result[None] = self.data.get(None, 0)
            for var, coeff in self.data.items():
                if var is not None:
                    result[var] = -self.data[var]
                    result[None] += self.data[var]
            return NormalizedLinearSum(result)

    @property
    def lhs(self):
        return [ (abs(coeff), mkNormalizedVar(var) if coeff > 0 else ~mkNormalizedVar(var)) for var, coeff in self.data.items() if var is not None ]

    @property
    def rhs(self):
        normalization = sum((coeff for var,coeff in self.data.items() if coeff < 0 and var is not None));
        return -self.data.get(None, 0) - normalization

    def asOPB(self, isObjective = False):
        result = " ".join("%i %s"%(coeff, var.get_opb_name()) for var, coeff in self.data.items() if var is not None)
        if not isObjective:
            result += " >= %i ;"%( -self.data.get(None, 0) )
        return result

    def asCNF(self):
        if computeNormalizedRhs(self.data) != 1:
            raise ValueError("Try to convert non clause to CNF.")

        result = " ".join("%s%s"%("~" if coeff < 0 else "", var) for var, coeff in self.data.items() if var is not None)
        result += " 0"
        return result
