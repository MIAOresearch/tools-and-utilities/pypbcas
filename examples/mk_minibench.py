from app_sequential import main as encode
from veripb import run as veripb
from time import perf_counter
import os



def main():
    split_rup = True
    assume_eq = True

    printHeader = not os.path.exists("results.csv")

    with open("results.csv", "a") as csv:
        if printHeader:
            print("split_rup,assume_eq,num_vars,time", file=csv)

        config = 0
        for assume_eq in [True, False]:
            for split_rup in [True, False]:
                config += 1
                for i in range(200, 300, 10):
                    print(config, i)
                    encode(i, assume_eq, split_rup, False)
                    with open("sequential_counter.opb", "r") as opb, open("sequential_counter.pbp","r") as pbp:
                        start = perf_counter()
                        veripb(opb,pbp)
                        time = perf_counter() - start

                    print(split_rup,assume_eq,i,time, sep=",", file=csv)


if __name__ == '__main__':
    main()