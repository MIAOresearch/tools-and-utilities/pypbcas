from pbcas.ast import *
from math import log2, ceil

from pbcas.prooflogging import Proof
from pbcas.opb_formula import OPBFormula
from pbcas.cnf_formula import CNFFormula
from pbcas.timed_function import TimedFunction

from app_totalizer import TotalizerEncoder


# Global Polynomial Watchdog Encoding, see
#   Olivier Bailleux, Yacine Boufkhad, Olivier Roussel:
#   New Encodings of Pseudo-Boolean Constraints into CNF. SAT 2009: 181-194
# Currently uses comparator instead of adding constant to multiple of highes power.
# No proof logging support.

def intToBits(value, nBits):
    mask = 1 << nBits
    for i in range(nBits):
        mask >>= 1
        yield bool(value & mask)

class App:
    def __init__(self, context):
        self.context = context



    def xor_of_unary(self, unary_vars, out_name):
        def val_to_vars(val):
            result = 0
            if val > 0:
                result = result + ~unary_vars[val - 1]
            if val < len(unary_vars):
                result = result + unary_vars[val]
            return result

        out = self.context.freshVar(out_name)

        for val in range(len(unary_vars) + 1):
            if val % 2 == 0:
                C = val_to_vars(val) + ~out >= 1
            else:
                C = val_to_vars(val) +  out >= 1

            self.context.cnf.add(C)

        return out


    def binaryGeq(self, x, b):
        name = lambda x: "cmp_%i"%(x)

        i = len(x) + 1
        compare_var = self.context.freshVar(name(i))
        C = compare_var >= 1
        self.context.cnf.add(C)


        previous = compare_var
        for xi,bi in zip(x,b):
            i -= 1
            if bi:
                C = ~previous + xi >= 1
                self.context.cnf.add(C)
            else:
                compare_var = self.context.freshVar(name(i))

                clauses = [
                    ~compare_var + ~xi >= 1,
                    ~compare_var + previous >= 1,
                    compare_var + xi + ~previous >= 1
                ]

                for C in clauses:
                    self.context.cnf.add(C)

                previous = compare_var



    def __call__(self, constraint):
        normalized = normalize_by_reduce(constraint)

        batch = dict()
        terms = normalized.lhs
        for coeff, var in terms:
            for i in range(ceil(log2(coeff)) + 1):
                if (coeff & 2**i) != 0:
                    batch.setdefault(i, list()).append(var)


        totalizer = TotalizerEncoder(self.context)

        # sum among level
        batch_out = dict()
        for key, values in batch.items():
            if len(values) > 1:
                batch_out[key] = [x for i,x in totalizer.sum_only(Add([1 * x for x in values]))]
            elif len(values) == 1:
                batch_out[key] = [values[0]]


        # sum across levels
        level_out = dict()
        previous = batch_out.get(0, list())
        level_out[0] = previous

        i = 1
        while i <= max(batch_out.keys()) or previous:
            previous_half = list()

            j = 1
            while j < len(previous):
                previous_half.append(previous[j])
                j += 2

            if i in batch_out:
                names = lambda x: "z_%i"%i
                _, _, terms = totalizer.handle_block([(1,x) for x in previous_half], [(1,x) for x in batch_out[i]], names)
                previous = [x for i,x in terms]
            else:
                previous = previous_half

            level_out[i] = previous

            i += 1
        num_levels = i - 1


        # compute binary value
        out_vars = list()
        for i in reversed(range(num_levels)):
            out_vars.append(self.xor_of_unary(level_out[i], "out_%i"%(i)))

        # compare binary value
        k = constraint.rhs

        assert(k <= 2**(len(out_vars) + 1) - 1 and "Constraint is unsatisfiable!!!")

        self.binaryGeq(out_vars, intToBits(k, len(out_vars)))



def main():

    from pbcas.prooflogging import Proof
    from encoder_context import EncoderContext
    from pbcas.opb_formula import OPBFormula
    from pbcas.cnf_formula import CNFFormula
    import random

    # begin options
    seed = random.randint(0,2**64)
    print("seed", seed)
    random.seed(seed)
    n = 10
    coeffs = [random.randint(1,2**32) for i in range(n)]
    k = sum(coeffs) // 2
    print("rhs", k)
    useFancyNames = False
    # end options

    constraint = sum([a * Variable("x%i"%i) for i,a in enumerate(coeffs, start = 1)]) >= k
    constraint = normalize_by_reduce(constraint)
    print_asci(constraint)

    if not useFancyNames:
        rename = Variable2IntRenaming()
    else:
        rename = lambda x: x

    formula = OPBFormula(rename = rename)
    formula.add(constraint)

    # card = sum([a * Variable("x%i"%i) for a,i in enumerate(coeffs)]) >= k
    # formula.add( card )

    with open("sequential_counter.opb", "w") as opb_file:
        formula.write(opb_file)

    with open("sequential_counter.pbp", "w") as proof_file:
        proof = Proof(proof_file, formula.num_inequalities(), rename = rename)
        context = EncoderContext(proof)

        encode = App(context)
        encode(constraint)

    with open("sequential_counter.cnf", "w") as cnf_file:
        context.cnf.rename = rename
        context.cnf.write(cnf_file)

    with open("test.opb", "w") as test_file:
        obj = sum([-a * x for a,x in constraint.lhs])
        test = OPBFormula(objective = constraint.lhs, rename = Variable2IntRenaming())
        test.add_all(context.cnf)
        test.write(test_file)


if __name__ == '__main__':
    main()