from setuptools import setup

setup(
    name='pbcas',
    version='0.1',
    description='Helper library for PB problems.',
    url='https://gitlab.com/MIAOresearch/pypbcas',
    author='Stephan Gocht',
    author_email='stephan@gobro.de',
    license='MIT',
    packages=['pbcas'],
    setup_requires=[],
    install_requires=[],
    entry_points= {},
)
